package android.strotze.todolist;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Student on 11/4/2015.
 */
public class ItemFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
        this.activity = (MainActivity) activity;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View layoutView = inflater.inflate(R.layout.onpostselected, container, false);
       TextView tv1 = (TextView)layoutView.findViewById(R.id.ellie);
       TextView tv2 = (TextView)layoutView.findViewById(R.id.ellie2);
       TextView tv3 = (TextView)layoutView.findViewById(R.id.ellie3);
       TextView tv4 = (TextView)layoutView.findViewById(R.id.ellie4);

       tv1.setText("Name: " + activity.todoitems.get(activity.currentItem).title);
       tv2.setText("Name: " + activity.todoitems.get(activity.currentItem).dateAdded);
       tv3.setText("Name: " + activity.todoitems.get(activity.currentItem).dateDue);
       tv4.setText("Name: " + activity.todoitems.get(activity.currentItem).homework);

        return layoutView;
    }

}






