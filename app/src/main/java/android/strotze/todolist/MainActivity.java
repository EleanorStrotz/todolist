package android.strotze.todolist;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActitvity implements ActivityCallback{
    public ArrayList<todoitem> todoitems = new ArrayList<todoitem>();
    public int currenttime;
    public int currentItem;


    @Override
    protected Fragment createFragment() {
        return new todoFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ItemFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}
