package android.strotze.todolist;


public class todoitem {
    public String title;
    public String homework;
    public String dateAdded;
    public String dateDue;

    public todoitem(String title, String dateAdded, String dateDue, String homework) {
        this.title = title;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.homework = homework;
    }
}
