package android.strotze.todolist;


import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
