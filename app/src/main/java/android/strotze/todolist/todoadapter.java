package android.strotze.todolist;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class todoadapter extends RecyclerView.Adapter<todoholder>{
    private ArrayList<todoitem> todoitems;
    private ActivityCallback activityCallback;

    public todoadapter(ActivityCallback activityCallback, ArrayList<todoitem> todoitems){
        this.todoitems = todoitems;
        this.activityCallback = activityCallback;
    }

    @Override
    public todoholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new todoholder(view);
    }

    @Override
    public void onBindViewHolder(todoholder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCallback.onPostSelected(redditUri);
            }
        });
        holder.titleText.setText(todoitems.get(position).title);
    }

    @Override
    public int getItemCount() {
        return todoitems.size();
    }
}
