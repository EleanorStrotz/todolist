package android.strotze.todolist;

    import android.app.Activity;
    import android.os.Bundle;
    import android.support.annotation.Nullable;
    import android.app.Fragment;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;


public class todoFragment extends Fragment {

    private RecyclerView recyclerView;
            private ActivityCallback activityCallback;
           private MainActivity activity;

                @Override
            public void onAttach(Activity activity) {
                    super.onAttach(activity);
                    activityCallback = (ActivityCallback)activity;
                   this.activity = (MainActivity)activity;
                }

                    @Override
            public void onDetach() {
                    super.onDetach();
                    activityCallback = null;
               }

                   @Nullable
           @Override

            public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                   View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

                       recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
                       recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                       todoitem i1 = new todoitem("hi", "hello", "adios", "amigos");
                       todoitem i2 = new todoitem("I am almost done", "auntie", "done", "always");
                       todoitem i3 = new todoitem("friends", "good", "nope", "yep");
                       todoitem i4 = new todoitem("paola", "gonzalez", "nope", "yep");
                       todoitem i5 = new todoitem("hello", "adele", "times", "bitt");





                       activity.todoitems.add(i1);
                       activity.todoitems.add(i2);
                       activity.todoitems.add(i3);
                       activity.todoitems.add(i4);
                       activity.todoitems.add(i5);


                               todoadapter adapter = new todoadapter(activityCallback, activity.todoitems);
                       recyclerView.setAdapter(adapter);

                           return view;
                   }


}
